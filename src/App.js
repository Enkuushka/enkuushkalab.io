import {
  DropDownNav,
  Presentation,
  PresenterModePlugin,
  Slide,
} from "react-presents";
import "./App.css";

// Automatically load all slides in the Slides folder
const slides = require
  .context("./slides/", false, /\.js$/)
  .keys()
  .map((filename) => filename.replace("./", ""))
  .map((filename) => require(`./slides/${filename}`).default);

// Support navigating to any slides also tagged with a :title
const options = slides
  .map((slide, index) => ({
    label: slide.title,
    value: index,
  }))
  .filter((option) => option.label);

function App() {
  return (
    <Presentation>
      <PresenterModePlugin />

      {slides.map((Component, index) => (
        <Slide component={Component} key={index} />
      ))}

      <DropDownNav key="DropDownNav" options={options} />
    </Presentation>
  );
}

export default App;
