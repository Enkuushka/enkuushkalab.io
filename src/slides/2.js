import React from "react";
import { ContentSlide, Step } from "react-presents";
import { Link } from "react-router-dom";
import libraryImage from "../images/library.png";
import frameworkImage from "../images/framework.jpeg";

const slide = () => (
  <ContentSlide>
    <h1>{slide.title}</h1>

    <div className="main-table">
      <div>
        <h2>Library</h2>
        <Step index={1}>
          <img
            src={libraryImage}
            alt="gears as example of library"
            className="big-image"
          />
          <p>
            React, React-router, React-presents
          </p>
        </Step>
      </div>
      <div>
        <h2>Framework</h2>
        <Step index={2}>
          <img
            src={frameworkImage}
            alt="framework example"
            className="big-image"
          />
          <p>
            Next.js, Gatsby.js
          </p>
        </Step>
      </div>
    </div>
  </ContentSlide>
);

slide.title = "Library юу эсвэл framework үү?";

export default slide;
