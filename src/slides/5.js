import React from "react";
import { ContentSlide, Step } from "react-presents";

import uiImage from "../images/react-component-ui.png";
import componentDiagram from "../images/react-component-diagram.png";

const slide = ({ stepIndex }) => (
  <ContentSlide>
    <h1>{slide.title}</h1>

    <div className="diagram-presenter">
      <Step index={1}>
        <img className={`front-image ${ stepIndex == 2 ? "move":"" }`} src={uiImage} alt='web component UI'/>
      </Step>

      <Step index={2}>
        <img className="back-image" src={componentDiagram} alt='react component Diagram'/>
      </Step>
    </div>
  </ContentSlide>
);

slide.title = "Component diagram";

export default slide;
