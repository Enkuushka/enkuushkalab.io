import React from "react";
import { TitleSlide } from "react-presents";

const slide = () => (
  <TitleSlide>
    <h1>{slide.title} 🙂 </h1>
  </TitleSlide>
);

slide.title = "Баярлалаа";

export default slide;