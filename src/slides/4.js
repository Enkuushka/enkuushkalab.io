import React from "react";
import { ContentSlide, Step } from "react-presents";
import { Link } from "react-router-dom";

const slide = () => (
  <ContentSlide>
    <h1>{slide.title}</h1>

    <ol>
      <Step index={1}>
        <li>
          JSX - HTML шиг харагддаг JS
          <Link className="btn-link" to="example-1">
            Жишээ
          </Link>
        </li>
      </Step>
      <Step index={2}>
        <li>
          IF, LOOP - Нөхцөл, давталт
          <Link className="btn-link" to="example-2">
            Жишээ
          </Link>
          <Link className="btn-link" to="example-4">
            Жишээ
          </Link>
        </li>
      </Step>
      <Step index={3}>
        <li>
          Component - Компонент
          <Link className="btn-link" to="example-3">
            Жишээ
          </Link>
        </li>
      </Step>
      <Step index={4}>
        <li>
          Props - Гаднаас тохируулдаг утгууд
          <Link className="btn-link" to="example-4">
            Жишээ
          </Link>
        </li>
      </Step>
      <Step index={5}>
        <li>
          State - Төлөв
          <Link className="btn-link" to="example-6">
            Жишээ
          </Link>
        </li>
      </Step>
      <Step index={6}>
        <li>
          Event - Үйлдэлд хариу үзүүлэх
          <Link className="btn-link" to="example-6">
            Жишээ
          </Link>
        </li>
      </Step>
    </ol>
  </ContentSlide>
);

slide.title = "Үндсэн хэсгүүд";

export default slide;
