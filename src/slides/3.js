import React from "react";
import { ContentSlide, Step } from "react-presents";
import { Prism as SyntaxHighlighter } from "react-syntax-highlighter";
import { dark, coyWithoutShadows } from "react-syntax-highlighter/dist/esm/styles/prism";

const commands = `npx create-react-app my-app
cd my-app
npm start`;

const code = `import React from 'react';
import ReactDOM from 'react-dom/client';

const root = ReactDOM.createRoot(document.getElementById('root'));
const element = <h1>Hello, world</h1>;
root.render(element);`;

const slide = () => (
  <ContentSlide>
    <h1>{slide.title}</h1>

    <SyntaxHighlighter
      language="bash"
      style={dark}
    >
      { commands }
    </SyntaxHighlighter>

    <Step index={1}>
      <h3>Жижиг код</h3>
    <SyntaxHighlighter
      language="javascript"
      style={coyWithoutShadows}
      showLineNumbers={true}
    >
      { code }
    </SyntaxHighlighter>
    </Step>
  </ContentSlide>
);

slide.title = "Installing and starting";

export default slide;
