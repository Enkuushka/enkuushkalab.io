import React from 'react'
import { TitleSlide } from 'react-presents'

const slide = () => (
  <TitleSlide>
    <h1>{slide.title}</h1>
    <p>
      <a href="https://reactjs.org/docs">https://reactjs.org/docs</a> - г нуршин ярина
    </p>
  </TitleSlide>
)

slide.title = 'React.js'

export default slide  