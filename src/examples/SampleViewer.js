import React, { Component } from 'react';
import { Link } from 'react-router-dom';

import { Prism as SyntaxHighlighter } from 'react-syntax-highlighter';
import { coyWithoutShadows } from 'react-syntax-highlighter/dist/esm/styles/prism';

function SampleViewer(props) {
  const codeStr = props.codeStr;
  const component = props.component;
  return (
  <div className="sample-viewer">
      <Link className='btn-link' to={-1}>
        back to slides
      </Link>
      <div className="main-pane">
        <div className="code">
          <SyntaxHighlighter 
            language="javascript" 
            style={coyWithoutShadows}
            showLineNumbers={true}
          >
            { codeStr }
          </SyntaxHighlighter>
        </div>
        <div className="sample">
            { component() }
        </div>
      </div>
    </div>
  );
}

export default SampleViewer;
