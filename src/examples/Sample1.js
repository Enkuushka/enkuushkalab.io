import React from 'react';

function Sample1() {
  const element = <h1>Hello, world</h1>;

  return (
    <div>
      Hello from the JSX!
      <hr />
      { element }
    </div>
  );
}

export default Sample1;
