import React from 'react';
import ReactDOM from 'react-dom/client';
import { BrowserRouter, Routes, Route } from 'react-router-dom';
import './index.css';
import App from './App';
import Sample1 from './examples/Sample1';
import reportWebVitals from './reportWebVitals';
import Greeting from './examples/Sample2';
import NumberList from './examples/Sample3';
import Calculator from './examples/Sample4';
import HookExample from './examples/Sample5';
import SampleViewer from './examples/SampleViewer';

import code1 from '!!raw-loader!./examples/Sample1.js';
import code2 from '!!raw-loader!./examples/Sample2.js';
import code3 from '!!raw-loader!./examples/Sample3.js';
import code4 from '!!raw-loader!./examples/Sample4.js';
import code5 from '!!raw-loader!./examples/Sample5.js';
import code6 from '!!raw-loader!./examples/Sample5.js';

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <BrowserRouter>
    <Routes>
      <Route path="/" element={<App />} />
      <Route path="example-1" element={<SampleViewer component = { () => <Sample1 /> } codeStr={ code1 } />} />
      <Route path="example-2" element={<SampleViewer component = { () => <Greeting isLoggedIn={true} /> } codeStr={ code2 } />} />
      <Route path="example-3" element={<SampleViewer component = { () => <Greeting isLoggedIn={false} /> } codeStr={ code2 } />} />
      <Route path="example-4" element={<SampleViewer component = { () => <NumberList numbers={[1, 2, 3, 4, 5]} /> } codeStr={ code3 } />} />
      <Route path="example-5" element={<SampleViewer component = { () => <Calculator /> } codeStr={ code4 } />} />
      <Route path="example-6" element={<SampleViewer component = { () => <><HookExample /> <HookExample /></> } codeStr={ code6 } />} />
    </Routes>
  </BrowserRouter>
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
